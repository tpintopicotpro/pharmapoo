import Client from './Client';
import Medicament from './Medicament';

export default class Main
{
    private clients: Client[] = []; // : Client[] indique que la propriete privée clients ne peut contenir que des objets de type Client
    private medicaments: Medicament[] = []; // pareil pour les medicaments

    ajouterClient(nom: string, credit: number)
    {
        const nouveauClient: Client = new Client(nom, credit);

        this.clients.push(nouveauClient);

        console.log(this.clients)
    }
}